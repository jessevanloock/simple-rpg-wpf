﻿using System;
using System.Collections.Generic;
using System.Text;
using Engine.Models;
using Engine.Factories;
using System.ComponentModel;
using System.Linq;
using Engine.EventArgs;

namespace Engine.Controllers
{
    public class GameSession : BaseNotificationClass
    {
        public event EventHandler<GameMessageEventArgs> onMessageRaised;

        private Player _currentPlayer;
        private Location _currentLocation;
        private Monster _currentMonster;
        private Trader _currentTrader;
        public bool hasMonster => currentMonster != null;
        public bool hasTrader => currentTrader != null;
        public Player currentPlayer {
            get {
                return _currentPlayer;
            }
            set { 
                if(_currentPlayer != null)
                {
                    _currentPlayer.onKilled -= OnCurrentPlayerKilled;
                    _currentPlayer.OnLeveledUp -= OnCurrentPlayerLeveledUp;
                }

                _currentPlayer = value;

                if (_currentPlayer != null)
                {
                    _currentPlayer.onKilled += OnCurrentPlayerKilled;
                    _currentPlayer.OnLeveledUp += OnCurrentPlayerLeveledUp;
                }
            }
        }
        public Location currentLocation
        {
            get { return _currentLocation; }
            set
            {
                _currentLocation = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(hasLocationToNorth));
                OnPropertyChanged(nameof(hasLocationToWest));
                OnPropertyChanged(nameof(hasLocationToEast));
                OnPropertyChanged(nameof(hasLocationToSouth));

                CompleteQuestsAtLocation();
                GivePlayerQuestsAtLocation();
                GetMonsterAtLocation();

                currentTrader = currentLocation.traderHere;
            }

        }

        public World currentWorld { get;}

        public Monster currentMonster { 
            get { return _currentMonster; } 
            set {

                if (_currentMonster != null)
                {
                    _currentMonster.onKilled -= OnCurrentMonsterKilled;
                }

                _currentMonster = value;

                if (_currentMonster != null)
                {
                    _currentMonster.onKilled += OnCurrentMonsterKilled;
                    RaiseMessage("");
                    RaiseMessage($"You see a {currentMonster.name} here!");
                }
                OnPropertyChanged();
                OnPropertyChanged(nameof(hasMonster));
            } 
        }

        public Trader currentTrader
        {
            get { return _currentTrader; }
            set
            {
                _currentTrader = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(hasTrader));
            }
        }

        public Weapon currentWeapon { get; set; }

        public bool hasLocationToNorth => currentWorld.LocationAt(currentLocation.xCoordinate, currentLocation.yCoordinate + 1) != null;
    
        public bool hasLocationToWest => currentWorld.LocationAt(currentLocation.xCoordinate - 1, currentLocation.yCoordinate) != null;

        public bool hasLocationToEast => currentWorld.LocationAt(currentLocation.xCoordinate + 1, currentLocation.yCoordinate) != null;
        public bool hasLocationToSouth => currentWorld.LocationAt(currentLocation.xCoordinate, currentLocation.yCoordinate - 1) != null;
        public GameSession()
        {
            currentPlayer = new Player("Jesse","Fighter",0,10,10,1000000);

            if (!currentPlayer.weapons.Any())
            {
                currentPlayer.AddItemToInventory(GameItemFactory.CreateGameItem(1001));
            }

            currentWorld = WorldFactory.CreateWorld();
            currentLocation = currentWorld.LocationAt(0, -1);
        }

        public void MoveNorth()
        {
            if (hasLocationToNorth)
            {
                currentLocation = currentWorld.LocationAt(currentLocation.xCoordinate, currentLocation.yCoordinate + 1);
            }
        }

        public void MoveWest()
        {
            if (hasLocationToWest)
            {
                currentLocation = currentWorld.LocationAt(currentLocation.xCoordinate - 1, currentLocation.yCoordinate);
            }
        }

        public void MoveEast()
        {
            if (hasLocationToEast)
            {
                currentLocation = currentWorld.LocationAt(currentLocation.xCoordinate + 1, currentLocation.yCoordinate);
            }
        }

        public void MoveSouth()
        {
            if (hasLocationToSouth)
            {
                currentLocation = currentWorld.LocationAt(currentLocation.xCoordinate, currentLocation.yCoordinate - 1);
            }
        }

        private void CompleteQuestsAtLocation()
        {
            foreach(Quest quest in currentLocation.questsAvailableHere)
            {
                QuestStatus questToComplete = currentPlayer.quests.FirstOrDefault(playerQuest => playerQuest.playerQuest.id == quest.id && !playerQuest.isCompleted);

                if(questToComplete != null)
                {
                    if (currentPlayer.HasAllTheseItems(quest.itemsToComplete)){
                        foreach(ItemQuantity itemQuantity in quest.itemsToComplete)
                        {
                            for(int i = 0; i < itemQuantity.quantity; i++)
                            {
                                currentPlayer.RemoveItemFromInventory(currentPlayer.inventory.FirstOrDefault(item => item.itemTypeId == itemQuantity.itemId));
                            }
                        }

                        RaiseMessage("");
                        RaiseMessage($"You completed the '{quest.name}' quest.");

                        // Give the player the quest rewards
                        RaiseMessage($"You receive {quest.rewardExperiencePoints} experience points.");
                        currentPlayer.AddExperience(quest.rewardExperiencePoints);

                        RaiseMessage($"You receive {quest.rewardGold} gold.");
                        currentPlayer.ReceiveGold(quest.rewardGold);

                        foreach (ItemQuantity itemQuantity in quest.rewardItems)
                        {
                            for (int i = 0; i < itemQuantity.quantity; i++)
                            {
                                GameItem rewardItem = GameItemFactory.CreateGameItem(itemQuantity.itemId);

                                RaiseMessage($"You receive a {rewardItem.name}");
                                currentPlayer.AddItemToInventory(rewardItem);
                            }
                        }

                        questToComplete.isCompleted = true;
                    }
                }
            }
        }

        private void GivePlayerQuestsAtLocation()
        {
            foreach(Quest locationQuest in currentLocation.questsAvailableHere)
            {
                if(!currentPlayer.quests.Any(playerQuest => playerQuest.playerQuest.id == locationQuest.id))
                {
                    RaiseMessage("");
                    RaiseMessage($"You receive the '{locationQuest.name}' quest.");
                    RaiseMessage(locationQuest.description);
                    currentPlayer.quests.Add(new QuestStatus(locationQuest));

                    RaiseMessage("Return with:");
                    foreach(ItemQuantity item in locationQuest.itemsToComplete)
                    {
                        RaiseMessage($"     {item.quantity} x {GameItemFactory.CreateGameItem(item.itemId).name}");
                    }

                    RaiseMessage($"And you will receive:");
                    RaiseMessage($"     {locationQuest.rewardExperiencePoints} experience points");
                    RaiseMessage($"     {locationQuest.rewardGold} gold");
                    foreach (ItemQuantity item in locationQuest.rewardItems)
                    {
                        RaiseMessage($"     {item.quantity} x {GameItemFactory.CreateGameItem(item.itemId).name}");
                    }
                }
            }
        }

        private void GetMonsterAtLocation()
        {
            currentMonster = currentLocation.GetMonster();
        }

        public void AttackCurrentMonster()
        {
            if (currentWeapon == null)
            {
                RaiseMessage("You must select a weapon, to attack.");
                return;
            }

            // Determine damage to monster
            int damageToMonster = RandomNumberGenerator.NumberBetween(currentMonster.minimumDamage, currentMonster.maximumDamage);

            if (damageToMonster == 0)
            {
                RaiseMessage($"You missed the {currentMonster.name}.");
            }
            else
            {
                RaiseMessage($"You hit the {currentMonster.name} for {damageToMonster} hit points.");
                currentMonster.TakeDamage(damageToMonster);
            } 
            if (currentMonster.isDead)
            {
                // Get another monster to fight
                GetMonsterAtLocation();
            }
            else {
                // if monster is still alive, let the monster attack
                int damageToPlayer = RandomNumberGenerator.NumberBetween(currentMonster.minimumDamage, currentMonster.maximumDamage);

                if(damageToPlayer == 0)
                {
                    RaiseMessage($"The {currentMonster.name} attacks, but misses you.");
                }
                else
                {
                    RaiseMessage($"The {currentMonster.name} hit you for {damageToPlayer} hit points.");
                    currentPlayer.TakeDamage(damageToPlayer);
                }
            }
        }

        private void OnCurrentPlayerKilled(object sender, System.EventArgs eventArgs)
        {
            RaiseMessage("");
            RaiseMessage($"You fainted!");

            currentLocation = currentWorld.LocationAt(0, -1); // Player's Home
            currentPlayer.CompletelyHeal(); // Heal to full health
        }

        private void OnCurrentMonsterKilled(object sender, System.EventArgs eventArgs)
        {
            RaiseMessage("");
            RaiseMessage($"You defeated the {currentMonster.name}");

            RaiseMessage($"You receive {currentMonster.rewardExperiencePoints} experience points.");
            currentPlayer.AddExperience(currentMonster.rewardExperiencePoints);

            RaiseMessage($"You receive {currentMonster.gold} gold.");
            currentPlayer.ReceiveGold(currentMonster.gold);

            foreach (GameItem monsterItem in currentMonster.inventory)
            {
                RaiseMessage($"You receive one {monsterItem.name}.");
                currentPlayer.AddItemToInventory(monsterItem);
            }
        }
        private void OnCurrentPlayerLeveledUp(object sender, System.EventArgs eventArgs)
        {
            RaiseMessage($"You are now level {currentPlayer.level}!");
        }
        private void RaiseMessage(string message)
        {
            onMessageRaised?.Invoke(this, new GameMessageEventArgs(message));
        }
    }
}
