﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.EventArgs
{
    public class GameMessageEventArgs : System.EventArgs
    {
        public string message { get; private set; }

        public GameMessageEventArgs(string message)
        {
            this.message = message;
        }
    }
}
