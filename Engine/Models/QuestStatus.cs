﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Models
{
    public class QuestStatus : BaseNotificationClass
    {
        private bool _isCompleted;
        public Quest playerQuest { get;}
        public bool isCompleted { get { return _isCompleted; }
            set {
                _isCompleted = value;
                OnPropertyChanged();
            }
        }

        public QuestStatus(Quest playerQuest)
        {
            this.playerQuest = playerQuest;
            this.isCompleted = false;
        }
    }
}
