﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Models
{
    public class GameItem
    {
        public int itemTypeId { get;}
        public string name { get; }
        public int price { get; }
        public bool isUnique { get; }

        public GameItem(int itemTypeId, string name, int price, bool isUnique = false)
        {
            this.itemTypeId = itemTypeId;
            this.name = name;
            this.price = price;
            this.isUnique = isUnique;
        }

        public GameItem Clone()
        {
            return new GameItem(itemTypeId, name, price, isUnique);
        }
    }
}
