﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Models
{
    public class Weapon : GameItem
    {
        public int minimumDamage { get; }
        public int maximumDamage { get;}

        public Weapon(int itemTypeId, string name, int price, int minimumDamage, int maximumDamage) : base(itemTypeId, name, price, true)
        {
            this.minimumDamage = minimumDamage;
            this.maximumDamage = maximumDamage;
        }

        public new Weapon Clone()
        {
            return new Weapon(itemTypeId, name, price, minimumDamage, maximumDamage);
        }
    }
}
