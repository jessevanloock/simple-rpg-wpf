﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;

namespace Engine.Models
{
    public abstract class LivingEntity : BaseNotificationClass
    {
        private string _name;
        private int _currentHitPoints;
        private int _maximumHitPoints;
        private int _gold;
        private int _level;

        public string name
        {
            get { return _name; }
            private set
            {
                _name = value;
                OnPropertyChanged();
            }
        }
        public int currentHitPoints
        {
            get { return _currentHitPoints; }
            private set
            {
                _currentHitPoints = value;
                OnPropertyChanged();
            }
        }
        public int maximumHitPoints
        {
            get { return _maximumHitPoints; }
            protected set
            {
                _maximumHitPoints = value;
                OnPropertyChanged();
            }
        }
        public int gold
        {
            get { return _gold; }
            private set
            {
                _gold = value;
                OnPropertyChanged();
            }
        }
        public int level
        {
            get { return _level; }
            protected set
            {
                _level = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<GameItem> inventory { get;}
        public ObservableCollection<GroupedInventoryItem> groupedInventory { get;}
        public List<GameItem> weapons => inventory.Where(item => item is Weapon).ToList();

        public bool isDead => currentHitPoints <= 0;

        public event EventHandler onKilled;
        protected LivingEntity(string name, int currentHitPoints, int maximumHitPoints, int gold, int level = 1)
        {
            this.name = name;
            this.currentHitPoints = currentHitPoints;
            this.maximumHitPoints = maximumHitPoints;
            this.gold = gold;
            this.level = level;

            inventory = new ObservableCollection<GameItem>();
            groupedInventory = new ObservableCollection<GroupedInventoryItem>();
        }

        public void TakeDamage(int damage)
        {
            currentHitPoints -= damage;
            if (isDead)
            {
                currentHitPoints = 0;
                RaiseOnKilledEvent();
            }
        }

        public void heal(int heal)
        {
            currentHitPoints += heal;
            if(currentHitPoints > maximumHitPoints)
            {
                currentHitPoints = maximumHitPoints;
            }
        }

        public void CompletelyHeal()
        {
            currentHitPoints = maximumHitPoints;
        }

        public void ReceiveGold(int gold)
        {
            this.gold += gold;
        }

        public void SpendGold(int gold)
        {
            if(gold > this.gold)
            {
                throw new ArgumentOutOfRangeException($"{name} only has {this.gold} gold, and cannot spend {gold} gold.");
            }
            this.gold -= gold;
        }

        public void AddItemToInventory(GameItem item)
        {
            inventory.Add(item);
            if (item.isUnique)
            {
                groupedInventory.Add(new GroupedInventoryItem(item, 1));
            }
            else
            {
                if (!groupedInventory.Any(gi => gi.item.itemTypeId == item.itemTypeId))
                {
                    groupedInventory.Add(new GroupedInventoryItem(item, 0));
                }
                groupedInventory.First(gi => gi.item.itemTypeId == item.itemTypeId).quantity++;
            }
                
            OnPropertyChanged(nameof(weapons));

        }
        public void RemoveItemFromInventory(GameItem item)
        {
            inventory.Remove(item);

            GroupedInventoryItem groupedInventoryItemToRemove = item.isUnique ?
                groupedInventory.FirstOrDefault(gi => gi.item == item) :
                groupedInventory.FirstOrDefault(gi => gi.item.itemTypeId == item.itemTypeId);
                

            if(groupedInventoryItemToRemove != null)
            {
                if(groupedInventoryItemToRemove.quantity == 1)
                {
                    groupedInventory.Remove(groupedInventoryItemToRemove);
                }
                else
                {
                    groupedInventoryItemToRemove.quantity--;
                }
            }
            OnPropertyChanged(nameof(weapons));
        }

        private void RaiseOnKilledEvent()
        {
            onKilled?.Invoke(this, new System.EventArgs());
        }
    }
}
