﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Models
{
    public class Quest
    {
        public int id { get;}
        public string name { get;}
        public string description { get;}

        public List<ItemQuantity> itemsToComplete { get;}

        public int rewardExperiencePoints { get;}
        public int rewardGold { get;}
        public List<ItemQuantity> rewardItems { get;}

        public Quest(int id, string name, string description, List<ItemQuantity> itemsToComplete, int rewardExperiencePoints, int rewardGold, List<ItemQuantity> rewardItems)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.itemsToComplete = itemsToComplete;
            this.rewardExperiencePoints = rewardExperiencePoints;
            this.rewardGold = rewardGold;
            this.rewardItems = rewardItems;
        }
    }
}
