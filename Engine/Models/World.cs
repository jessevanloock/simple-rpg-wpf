﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Models
{
    public class World
    {
        private readonly List<Location> _locations = new List<Location>();
    
        internal void AddLocation(int xCoordinate, int yCoordinate, string name, string description, string imageName)
        {
            _locations.Add(new Location(xCoordinate,yCoordinate, name, description, imageName));
        }

        public Location LocationAt(int xCoordinate, int yCoordinate)
        {
            foreach(Location location in _locations)
            {
                if (location.xCoordinate == xCoordinate && location.yCoordinate == yCoordinate)
                {
                    return location;
                }
            }
            return null;
        }
    }
}
