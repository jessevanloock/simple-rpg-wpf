﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine.Models
{
    public class MonsterEncounter
    {
        public int monsterId { get;}
        public int chanceOfEncountering { get; set; }

        public MonsterEncounter(int monsterId, int chanceOfEncountering)
        {
            this.monsterId = monsterId;
            this.chanceOfEncountering = chanceOfEncountering;
        }
    }
}
