﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Engine.Factories;

namespace Engine.Models
{
    public class Location
    {
        public int xCoordinate { get;}
        public int yCoordinate { get;}
        public string name { get;}
        public string description { get;}
        public string imageName { get;}
        public List<Quest> questsAvailableHere { get;} = new List<Quest>();
        public List<MonsterEncounter> monsterHere { get;} = new List<MonsterEncounter>();
        public Trader traderHere { get; set; }

        public Location(int xCoordinate, int yCoordinate, string name, string description, string imageName)
        {
            this.xCoordinate = xCoordinate;
            this.yCoordinate = yCoordinate;
            this.name = name;
            this.description = description;
            this.imageName = imageName;
        }

        public void AddMonster(int monsterId, int chanceOfEncountering)
        {
            if(monsterHere.Exists(monster => monster.monsterId == monsterId))
            {
                monsterHere.First(monster => monster.monsterId == monsterId).chanceOfEncountering = chanceOfEncountering;
            }else
            {
                monsterHere.Add(new MonsterEncounter(monsterId, chanceOfEncountering));
            }
        }

        public Monster GetMonster()
        {
            if (!monsterHere.Any())
            {
                return null;
            }

            int totalChances = monsterHere.Sum(monster => monster.chanceOfEncountering);

            int randomNumber = RandomNumberGenerator.NumberBetween(1, totalChances);

            int runningTotal = 0;

            foreach(MonsterEncounter monsterEncounter in monsterHere)
            {
                runningTotal += monsterEncounter.chanceOfEncountering;
                if(randomNumber <= runningTotal)
                {
                    return MonsterFactory.GetMonster(monsterEncounter.monsterId);
                }
            }
            return MonsterFactory.GetMonster(monsterHere.Last().monsterId);
        }
    }
}
