﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace Engine.Models
{
    public class Player : LivingEntity
    {
        private string _characterClass;
        private int _experiencePoints;

        public string characterClass
        {
            get { return _characterClass; }
            set
            {
                _characterClass = value;
                OnPropertyChanged();
            }
        }

        public int experiencePoints 
        { get { return _experiencePoints; }
           private set { 
                _experiencePoints = value;
                OnPropertyChanged();
                SetLevelAndMaximumHitPoints();
                }
        }

        public ObservableCollection<QuestStatus> quests { get;}

        public event EventHandler OnLeveledUp;

        public Player(string name, string characterClass, int experiencePoints, int maximumHitPoints, int currentHitPoints, int gold) : base(name, currentHitPoints, maximumHitPoints, gold)
        {
            this.characterClass = characterClass;
            this.experiencePoints = experiencePoints;
            quests = new ObservableCollection<QuestStatus>();
        }

        public bool HasAllTheseItems(List<ItemQuantity> items)
        {
            foreach(ItemQuantity itemQuantity in items)
            {
                if(inventory.Count(item => item.itemTypeId == itemQuantity.itemId) < itemQuantity.quantity)
                {
                    return false;
                }
            }
            return true;
        }

        public void AddExperience(int experiencePoints)
        {
            this.experiencePoints += experiencePoints;
        }

        public void SetLevelAndMaximumHitPoints()
        {
            int originalLevel = level;
            level = (experiencePoints / 100) + 1;

            if(level != originalLevel)
            {
                maximumHitPoints = level * 10;
                OnLeveledUp?.Invoke(this, System.EventArgs.Empty);
            }
        }
    }
}
