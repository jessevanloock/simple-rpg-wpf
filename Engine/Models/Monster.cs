﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Engine.Models
{
    public class Monster : LivingEntity
    {
       
        public string imageName { get;}
        public int minimumDamage { get;}
        public int maximumDamage { get;}

        public int rewardExperiencePoints { get;}

        public Monster(string name,
                       string imageName,
                       int maximumHitPoints,
                       int hitPoints,
                       int minimumDamage,
                       int maximumDamage,
                       int rewardExperiencePoints,
                       int rewardGold) : base(name, hitPoints, maximumHitPoints, rewardGold)
        {
            this.imageName = $"D:\\Personal_Projects\\Simple-RPG\\Engine\\Images\\Monsters\\{imageName}";
            this.minimumDamage = minimumDamage;
            this.maximumDamage = maximumDamage;
            this.rewardExperiencePoints = rewardExperiencePoints;
        }
    }
}
