﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Engine.Models;

namespace Engine.Factories
{
    internal static class QuestFactory
    {
        private static readonly List<Quest> quests = new List<Quest>();

        static QuestFactory()
        {
            // Declare the items needed to complete the quests, and its reward items
            List<ItemQuantity> itemsToComplete = new List<ItemQuantity>();
            List<ItemQuantity> rewardItems = new List<ItemQuantity>();

            itemsToComplete.Add(new ItemQuantity(9001, 5));
            rewardItems.Add(new ItemQuantity(1002, 2));

            // Create the quest
            quests.Add(new Quest(1, "Clear the herb garden", "Defeat the snakes in the Herbalist's Garden", itemsToComplete, 25, 10, rewardItems));
        }

        internal static Quest GetQuestById(int id)
        {
            return quests.FirstOrDefault(quest => quest.id == id);
        }
    }
}
