﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Engine.Models;

namespace Engine.Factories
{
    public static class GameItemFactory
    {
        private static readonly List<GameItem> standardGameItems = new List<GameItem>();

        static GameItemFactory()
        {
            standardGameItems.Add(new Weapon(1001, "Pointy Stick", 1, 1, 2));
            standardGameItems.Add(new Weapon(1002, "Rusty Sword", 5, 1, 3));
            standardGameItems.Add(new GameItem(9001, "Snake fang", 1));
            standardGameItems.Add(new GameItem(9002, "Snake skin", 2));
            standardGameItems.Add(new GameItem(9003, "Rat tail", 1));
            standardGameItems.Add(new GameItem(9004, "Rat fur", 2));
            standardGameItems.Add(new GameItem(9005, "Spider fang", 1));
            standardGameItems.Add(new GameItem(9006, "Spider silk", 2));
        }

        public static GameItem CreateGameItem(int itemTypeId)
        {
            GameItem standardItem = standardGameItems.FirstOrDefault(gameItem => gameItem.itemTypeId == itemTypeId);

            if(standardItem == null)
            {
                return null;
            }

            if (standardItem is Weapon)
            {
                return (standardItem as Weapon).Clone();
            }

            return standardItem.Clone();
        }
    }
}
