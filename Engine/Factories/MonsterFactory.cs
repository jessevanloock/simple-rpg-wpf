﻿using System;
using System.Collections.Generic;
using System.Text;
using Engine.Models;

namespace Engine.Factories
{
    public static class MonsterFactory
    {
        public static Monster GetMonster(int id)
        {
            Monster monster;
            switch (id)
            {
                case 1:
                    monster = new Monster("Snake", "Snake.png", 4, 4, 1, 2, 5, 1);
                    AddLootItem(monster, 9001, 25);
                    AddLootItem(monster, 9002, 75);
                    break;

                case 2:
                    monster = new Monster("Rat", "Rat.png", 5, 5, 1, 2, 5, 1);
                    AddLootItem(monster, 9003, 25);
                    AddLootItem(monster, 9004, 75);
                    break;

                case 3:
                    monster = new Monster("Giant Spider", "GiantSpider.png", 10, 10, 1, 4, 10, 3);
                    AddLootItem(monster, 9005, 25);
                    AddLootItem(monster, 9006, 75);
                    break;

                default:
                    throw new ArgumentException(string.Format("MonsterType {0} does not exist", id));
            }

            return monster;
        }

        private static void AddLootItem(Monster monster, int itemId, int percentage)
        {
            if(RandomNumberGenerator.NumberBetween(1, 100) < percentage)
            {
                monster.inventory.Add(GameItemFactory.CreateGameItem(itemId));
            }
            
        }
    }
}
