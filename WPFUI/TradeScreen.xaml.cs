﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Engine.Controllers;
using Engine.Models;

namespace WPFUI
{
    /// <summary>
    /// Interaction logic for TradeScreen.xaml
    /// </summary>
    public partial class TradeScreen : Window
    {
        public GameSession session => DataContext as GameSession;
        public TradeScreen()
        {
            InitializeComponent();
        }
        private void OnClick_Sell(object sender, RoutedEventArgs args)
        {
            GroupedInventoryItem groupedItemToSell = ((FrameworkElement)sender).DataContext as GroupedInventoryItem;

            if (groupedItemToSell != null)
            {
                session.currentPlayer.ReceiveGold(groupedItemToSell.item.price);
                session.currentTrader.AddItemToInventory(groupedItemToSell.item);
                session.currentPlayer.RemoveItemFromInventory(groupedItemToSell.item);
            }
        }

        private void OnClick_Buy(object sender, RoutedEventArgs args)
        {
            GroupedInventoryItem groupedItemToBuy = ((FrameworkElement)sender).DataContext as GroupedInventoryItem;

            if (groupedItemToBuy != null)
            {
                if (session.currentPlayer.gold >= groupedItemToBuy.item.price)
                {
                    session.currentPlayer.SpendGold(groupedItemToBuy.item.price);
                    session.currentTrader.RemoveItemFromInventory(groupedItemToBuy.item);
                    session.currentPlayer.AddItemToInventory(groupedItemToBuy.item);
                    
                }
                else
                {
                    MessageBox.Show("You do not have enough gold.");
                }
            }
        }

        private void OnClick_Close(object sender, RoutedEventArgs args)
        {
            Close();
        }
    }
}
