﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Engine.Controllers;

namespace TestEngine.Controllers
{
    [TestClass]
    public class TestGameSession
    {
        [TestMethod]
        public void TestCreateGameSession()
        {
            GameSession gameSession = new GameSession();

            Assert.IsNotNull(gameSession.currentPlayer);
            Assert.AreEqual("Home", gameSession.currentLocation.name);
        }
        [TestMethod]
        public void TestPlayerMovesHomeAndIsCompletelyHealedOnKilled()
        {
            GameSession gameSession = new GameSession();
            gameSession.currentPlayer.TakeDamage(999);

            Assert.AreEqual("Home", gameSession.currentLocation.name);
            Assert.AreEqual(gameSession.currentPlayer.level * 10, gameSession.currentPlayer.currentHitPoints);
        }
    }
}
